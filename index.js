/* 
Javascript
PTB Assesment Week 5
*/

// Tuliskan sebuah fungsi yg mengembalikan nilai sesuai dengan "Expected output"

// Filter Data
const dataNama = ['Adi', 'Dani', 'Dodi', 'Syahifa']
const MencariDia = () => {
    // Carilah Nama dengan huruf pertama berawalan huruf 'D' pada data di atas
    // Expected output : ['Dani', 'Dodi']
};

MencariDia();

const MencariJumlah = () => {
    // Carilah Nama dengan jumlah hurufnya lebih dari 3
    // Expected output : ['Dani','Dodi','Syahifa']
};

MencariJumlah();

const dataNamaKembar = ['Adi', 'Adi', 'Dani', 'Dodi', 'Syahifa']
const MencariDataKembar = () => {
    // Carilah Nama yg mempunyai lebih dari satu nama dalam array
    // Expected output : ['Adi']
}